<?php
//-- Table: public.alumnos
//
//-- DROP TABLE IF EXISTS public.alumnos;
//
//CREATE TABLE IF NOT EXISTS public.alumnos
//(
//    id SERIAL not NULL,
//    nombre character varying(50) COLLATE pg_catalog."default",
//    apellido character varying(50) COLLATE pg_catalog."default",
//	edad INTEGER,
//    CONSTRAINT pk_alumnos PRIMARY KEY (id)
//)
//
//TABLESPACE pg_default;
//
//ALTER TABLE IF EXISTS public.alumnos
//    OWNER to postgres;

$dbconn = pg_connect("host=localhost port=5432 user=postgres password=postgres dbname=ejercicio4")
or die("No se ha podido conectar");

$names = array(
    'Allison',
    'Arthur',
    'Ana',
    'Alex',
    'Arlene',
    'Alberto',
    'Barry',
    'Bertha',
    'Bill',
    'Bonnie',
    'Bret',
    'Beryl',
    'Chantal',
    'Cristobal',
    'Claudette',
    'Charley',
    'Cindy',
    'Chris',
    'Dean',
    'Dolly');

$last_names = array(
    'Abbott',
    'Acevedo',
    'Acosta',
    'Adams',
    'Adkins',
    'Aguilar',
    'Aguirre',
    'Albert',
    'Alexander',
    'Alford',
    'Allen',
    'Allison',
    'Alston',
    'Alvarado',
    'Alvarez',
    'Anderson',
    'Andrews');

for($i=0; $i < 12; $i++) {
    $insert = pg_insert(
        $dbconn,
        "alumnos",
        array(
            "nombre" => $names[$i],
            "apellido" => $last_names[$i],
            "edad" => $i + 18,
        )
    );
}

function devuelve_mayor_menor($dbconn){
    $result = pg_query($dbconn, "SELECT * FROM alumnos ORDER BY edad desc limit 1;");
    $array = array();
    while($row = pg_fetch_row($result)) {
        $array[0] = " $row[0] ".$row[1]. " ". $row[2]." ". $row[3]."\n";
    }

    $result = pg_query($dbconn, "SELECT * FROM alumnos ORDER BY edad asc limit 1;");
    while($row = pg_fetch_row($result)) {
        $array[1] = " $row[0] ".$row[1]. " ". $row[2]." ". $row[3]."\n";
    }

    return $array;
}

$menor_mayor = devuelve_mayor_menor($dbconn);

echo "Mayor edad => $menor_mayor[0]\n";
echo "Menor edad => $menor_mayor[1]";


pg_close($dbconn);


