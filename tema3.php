<?php
function genera_codigo() {
    $characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $characters_length = strlen($characters);
    $random_string = '';
    for ($i = 0; $i < 4; $i++) {
        $random_string .= $characters[rand(0, $characters_length - 1)];
    }
    return $random_string;
}

$array = array();

for ($i=0; $i<500; $i++){
    $array[$i] = genera_codigo();
}

foreach ($array as $key => $value) {
    echo "$key => $value\n";
}